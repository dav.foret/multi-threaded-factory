#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <iostream>
#include <vector>
#include <string>
#include <time.h>
#include <unistd.h>
#include <sstream>

#define _PHASE_COUNT 6

class ThreadObj {
public:
	std::string name;
	int thread_id;
	int workplace;
	int leave_status;
	pthread_t thread;
};

enum place {
    NUZKY, VRTACKA, OHYBACKA, SVARECKA, LAKOVNA, SROUBOVAK, FREZA,
    _PLACE_COUNT
};
 
const char *place_str[_PLACE_COUNT] = {
    [NUZKY] = "nuzky",
    [VRTACKA] = "vrtacka",
    [OHYBACKA] = "ohybacka",
    [SVARECKA] = "svarecka",
    [LAKOVNA] = "lakovna",
    [SROUBOVAK] = "sroubovak",
    [FREZA] = "freza",
};
 
enum product {
    A, B, C,
    _PRODUCT_COUNT
};
 
const char *product_str[_PRODUCT_COUNT] = {
    [A] = "A",
    [B] = "B",
    [C] = "C",
};
 
const int worktimes[_PLACE_COUNT] = {100000, 200000, 150000, 300000, 400000, 250000, 500000};

int find_string_in_array(const char **array, int length, const char *what)
{
    for (int i = 0; i < length; i++)
	if (strcmp(array[i], what) == 0)
	    return i;
    return -1;
};

const int phase_map[_PRODUCT_COUNT][_PHASE_COUNT] = {{0, 1, 2, 3, 1, 4}, {1, 0, 6, 1, 4, 5}, {6, 1, 5, 1, 6, 4}};

///////////////////////////////////////////////////////////////////
//FACTORY INTERNALS
//____________________
//global for convenience
//everything is mutexed with with same object as the cond var
//EOF (end of fshift) if 1, workers will actively attempt to leave, checking certain conditions
////is my leave val 1 if yes leave
////is there some work available for me, if yes, do it
////else check if EOF 1, if yes check amount of working workers
////if EOF 0 go back to work      |if x>0 go back to waiting
////                              |else check if any work could be done with all other workers, if yes wait
////							  |else leave (as EOF 1, no new workers or work can arrive)
//FACILITIES ////
//-roster of workers, containing refs to their thread object and a name + workplace
//-EOF indicator
//-amount of workers currently working (possibility of new work indicator)
//-8x3 array corresponding to amount of products in that "bin"; as in products[product][phase]
//-amount of workplaces of certain type (see enums)
pthread_mutex_t factory_lock;
pthread_mutex_t output_lock;
pthread_cond_t factory_cond;
int ready_places[_PLACE_COUNT] = {0,0,0,0,0,0,0};
//Zeroth phase represents request for the product, seventh is the finished product - not included in available work inquiry
int parts[_PRODUCT_COUNT][_PHASE_COUNT + 1] = {{0,0,0,0,0,0}, {0,0,0,0,0,0}, {0,0,0,0,0,0}};
int EOS = 0;
int currently_working = 0;
std::vector<ThreadObj> workers;


////////////////////////////////////////////////////////////////////
int find_work(int workplace, int& product_num, int& phase_num) {
	for (int i = 5; i > -1; --i) {

		for (int n = 0; n < 3; ++n) {
			if(parts[n][i] > 0 && ready_places[phase_map[n][i]] > 0 && workplace == phase_map[n][i]) {
				product_num = n;
				phase_num = i;
				return 1;
			};
		};
	};
	return 0;
};

int work_possible() {
	int product, phase;

	for(auto it = workers.begin(); it != workers.end(); ++it) {
		if(find_work(it->workplace, product, phase) && it->leave_status == 0) {
			return 1;
		};
	};
	return 0;
};


void* worker_shift(void* name_ptr) {
	pthread_mutex_lock(&factory_lock);
	int product_num = 0;
	int phase_num = 0;
	int workplace;
	int leave;
	std::string name = *(std::string*)name_ptr;

	

	for(auto it = workers.begin(); it != workers.end(); ++it) {
		if(it->name.compare(name) == 0) {
			workplace = it->workplace;
			break;
		};
	};
	goto ENTRY;

	while(1) {
		pthread_mutex_lock(&factory_lock);
		ENTRY:
		//Add product, broadcast, do a work possibility check before waiting on cond var 
		++parts[product_num][phase_num];
		++ready_places[workplace];
		--currently_working;
		pthread_cond_broadcast(&factory_cond);
		//Check leave cond
		for(auto it = workers.begin(); it != workers.end(); ++it) {
			if(it->name.compare(name) == 0) {
				leave = it->leave_status;
			};
		};
		if(leave) {
			pthread_mutex_unlock(&factory_lock);
			break;
		};
		if(find_work(workplace, product_num, phase_num) == 0) {

			while(find_work(workplace, product_num, phase_num) == 0) {
				//No work found for us, check EOS and if so, if we can leave, also currently working
				if(EOS) {
					if(!currently_working && !work_possible()) {
						pthread_mutex_unlock(&factory_lock);
						goto EXIT;
					};
				};
				if(leave) {
					pthread_mutex_unlock(&factory_lock);
					goto EXIT;;
				};
				pthread_cond_wait(&factory_cond, &factory_lock);

				if(leave) {
					pthread_mutex_unlock(&factory_lock);
					goto EXIT;;
				};
			};
		};
		--ready_places[workplace];
		--parts[product_num][phase_num];
		++phase_num;
		++currently_working;
		pthread_mutex_lock(&output_lock);
		pthread_mutex_unlock(&factory_lock);

		
		//std::cout << name << ' ' << place_str[workplace] << ' ' << phase_num << ' ' << product_str[product_num] << '\n';
		printf("%s %s %d %s\n", name.c_str(), place_str[workplace], phase_num, product_str[product_num]);
		if(phase_num == 6) {
			//std::cout << "done " << product_str[product_num] << '\n';
			printf("done %s\n", product_str[product_num]);
		};
		pthread_mutex_unlock(&output_lock);

		usleep(worktimes[workplace]);
	};
	EXIT:
	delete (std::string*)name_ptr;
	pthread_exit(NULL);
};
///////////////////////
int main(int argc, char **argv)
{
	pthread_mutex_init(&factory_lock, NULL);
	pthread_mutex_init(&output_lock, NULL);
	pthread_cond_init(&factory_cond, NULL);
 
    while (1) {
		std::string line, cmd, arg1, arg2, arg3;

		if(!getline(std::cin, line)) {
			pthread_mutex_lock(&factory_lock);
			EOS = 1;
			pthread_cond_broadcast(&factory_cond);
			pthread_mutex_unlock(&factory_lock);
		    break;	
		};
		if(!line.size()) continue;

		std::stringstream ss(line);
		cmd = arg1 = arg2 = arg3 = "";
		ss >> cmd;
		ss >> arg1;
		ss >> arg2;
		ss >> arg3;
	 	//new worker
		if (strcmp(cmd.c_str(), "start") == 0 && !arg1.empty() && !arg2.empty() && arg3.empty()) {
			if(find_string_in_array(place_str, 7, arg2.c_str()) < 0) continue;
			pthread_mutex_lock(&factory_lock);
			std::string* tmp = new std::string(arg1);
			ThreadObj new_worker;
			pthread_t thread;
			new_worker.thread = thread;
			new_worker.leave_status = 0;
			std::string name_str(arg1);
			new_worker.name = name_str;
			new_worker.workplace = find_string_in_array(place_str, 7, arg2.c_str());
			--parts[0][0];
			++currently_working;
			--ready_places[new_worker.workplace];
			workers.push_back(new_worker);
			workers.back().thread_id = pthread_create(&workers.back().thread, NULL, worker_shift, tmp);
			//Workers will complete part of return cycle upon entry, so some factory vars have to be adjusted - var(s) of what the worker made is thread-local, but preset
			pthread_mutex_unlock(&factory_lock);

		} else if (strcmp(cmd.c_str(), "make") == 0 && !arg1.empty() && arg2.empty()) {
		    pthread_mutex_lock(&factory_lock);
		    //Zeroth phase is the order, phase 1 will be done and printed and so forth
		    ++parts[find_string_in_array(product_str, 3, arg1.c_str())][0];
		    pthread_cond_broadcast(&factory_cond);
		    pthread_mutex_unlock(&factory_lock);

		} else if (strcmp(cmd.c_str(), "end") == 0 && !arg1.empty() && arg2.empty()) {
			pthread_mutex_lock(&factory_lock);
			std::string name_str(arg1);
			for(auto it = workers.begin(); it != workers.end(); ++it) {
				if(it->name.compare(name_str) == 0) {
					it->leave_status = 1;
					break;
				};
			};
			pthread_mutex_unlock(&factory_lock);

		} else if (strcmp(cmd.c_str(), "add") == 0 && !arg1.empty() && arg2.empty()) {

			if(find_string_in_array(place_str, 7, arg1.c_str()) < 0) continue;
		    pthread_mutex_lock(&factory_lock);
		    ++ready_places[find_string_in_array(place_str, 7, arg1.c_str())];
		    pthread_cond_broadcast(&factory_cond);
		    pthread_mutex_unlock(&factory_lock);
		} else if (strcmp(cmd.c_str(), "remove") == 0 && !arg1.empty() && arg2.empty()) {

			if(find_string_in_array(place_str, 7, arg1.c_str()) < 0) continue;
		    pthread_mutex_lock(&factory_lock);
		    --ready_places[find_string_in_array(place_str, 7, arg1.c_str())];
		    pthread_mutex_unlock(&factory_lock);
		} else {
			pthread_mutex_lock(&output_lock);
		    fprintf(stderr, "Invalid command: %s\n", line.c_str());
		    pthread_mutex_unlock(&output_lock);
		}
    };
    for(auto it = workers.begin(); it != workers.end(); ++it) {
    	void** retval;
    	pthread_join(it->thread, retval);
    };
    pthread_mutex_destroy(&factory_lock);
    pthread_mutex_destroy(&output_lock);
    pthread_cond_destroy(&factory_cond);
};