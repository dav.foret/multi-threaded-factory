CXX=g++
CFLAGS=-I -Wall -Werror -g -O2 -lpthread
EXTRA_CFLAGS?=-fno-omit-frame-pointer -fsanitize=address -static-libasan
  
all: factory
  
factory: main.cpp utils.hpp utils.cpp
	$(CXX) $(CFLAGS) $(EXTRA_CFLAGS) utils.hpp utils.cpp main.cpp -o factory
